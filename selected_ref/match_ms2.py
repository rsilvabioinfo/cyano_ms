from pyteomics import mzxml
from pyteomics import mgf
from spectrum_alignment import *
import numpy as np
import pandas as pd
import os 

import requests
import json

target = ['Palmyramide A', 'Curacin D', 'Curacin A', 'Carmabins A-B', 'Barbamides', 'Lyngbyabellins', 'Columbamides', 'Apratoxin A']

def get_library(lib, peaks=False):
    base_url = "gnps.ucsd.edu"
    if peaks:
        datasets_url = 'https://' + base_url + '/ProteoSAFe/LibraryServlet?library=' + lib + '&showpeaks=true'
    else:
        datasets_url = 'https://' + base_url + '/ProteoSAFe/LibraryServlet?library=' + lib
    json_obj = json.loads(requests.get(datasets_url).text)
    return json_obj['spectra']

libs = ["GNPS-LIBRARY", "GNPS-SELLECKCHEM-FDA-PART1", "GNPS-SELLECKCHEM-FDA-PART2", "GNPS-NIH-NATURALPRODUCTSLIBRARY", 
	"GNPS-NIH-SMALLMOLECULEPHARMACOLOGICALLYACTIVE", "GNPS-FAULKNERLEGACY", "GNPS-EMBL-MCF",
	"GNPS-NIH-NATURALPRODUCTSLIBRARY_ROUND2_POSITIVE", "GNPS-NIH-NATURALPRODUCTSLIBRARY_ROUND2_NEGATIVE", 
	"GNPS-PRESTWICKPHYTOCHEM", "GNPS-NIH-CLINICALCOLLECTION1", "GNPS-NIH-CLINICALCOLLECTION2", 
	"NIST14", "MASSBANK", "MONA", "MASSBANKEU", "HMDB", "RESPECT", "CASMI", "GNPS-NIST14-MATCHES", "GNPS-COLLECTIONS-MISC",
	"DEREPLICATOR_IDENTIFIED_LIBRARY", "PNNL-LIPIDS-POSITIVE", "PNNL-LIPIDS-NEGATIVE", "MIADB", ] 

dflist_peaks = [pd.DataFrame(get_library(x, peaks=True)) for x in libs]  
dflib_peaks = pd.concat(dflist_peaks) 

dflib_peaks[['SpectrumID', 'Precursor_MZ', 'Charge', 'peaks_json', 'Scan']].head() 

fmgf = [x for x in os.listdir() if '.mgf' in x]

speclist = []
for i in range(len(fmgf)):
    with mgf.read(fmgf[i]) as reader:
        for spectrum in reader:
            speclist.append({fmgf[i][:18] : [spectrum['params']['pepmass'][0], list(zip(spectrum['m/z array'], spectrum['intensity array']))]} )


def get_ms2df(file_name, speclist, pmztol=0.1, fragtol=0.1):
    rt = []
    mz = []
    inten = []
    prec_mz = []
    scan_num = []
    scores = []
    specids = []
    pmlist = []
    for spec in speclist:
        for k,v in spec.items():
            pmlist.append(v[0])
    pmlist = np.array(pmlist)
    with mzxml.read(file_name) as reader:
        for spectrum in reader:
                if spectrum['msLevel'] == 2:
                    p_mz = spectrum['precursorMz'][0].get('precursorMz')
                    loc = np.where(abs(pmlist-p_mz) < pmztol)[0]
                    if len(loc):
                        for i in loc:
                            qmz = spectrum['m/z array']
                            qint = spectrum['intensity array']
                            qspec = list(zip(qmz, qint))
                            pm1, spec1 = list(speclist[i].values())[0] 
                            specid = list(speclist[i].keys())[0] 
                            score = score_alignment(spec1, qspec, pm1, p_mz, fragtol)[0] 
                            if score>0:
                                lqmz = len(qmz)
                                scan_num.extend([spectrum['num']] * lqmz) 
                                rt.extend([spectrum['retentionTime']] * lqmz)
                                mz.extend(qmz)
                                inten.extend(qint)
                                prec_mz.extend([p_mz] * lqmz)
                                scores.extend([score] * lqmz)
                                specids.extend([specid] * lqmz)
    
    ms2_data = pd.DataFrame(
            {'prec_mz': prec_mz,
             'mz': mz,
             'inten': inten,
             'rt': rt,
             'scan_num': scan_num,
    	 'specids': specids,
             'scores': scores
            })
    return ms2_data


ms2_data = get_ms2df('1942_027E6_RE6_01_15219.mzXML', speclist) 

ms2_data.head()
ms2_data.groupby(['scan_num'])
ms2_data.groupby(['scan_num'])['scores'].max()

ms2_data[ms2_data['scores']==ms2_data['scores'].max()]['specids'].tolist()[0] 
