#!/home/rsilva/miniconda2/envs/gnps/bin/python

import logging
import requests
import zipfile
import os
import glob 
import re
import time
import numpy as np
import pandas as pd
import sys
from collections import Counter
import matplotlib
logging.getLogger("import matplotlib").setLevel(logging.ERROR)
matplotlib.use('agg')
from tanglegram import tangle
import scipy.sparse
from scipy.spatial.distance import squareform, pdist, jaccard
from skbio.stats.distance import mantel
from sklearn.metrics import jaccard_similarity_score
import itertools
from itertools import product
import multiprocessing
from optparse import OptionParser
#import seaborn as sns


def getSampID(fname):
     return [re.sub('\.mzXML.+$', '', x) for x in fname.split('###') if x!='']

def check_fname(net, cid, fn, tabgnps, mids=None):
    allcid = list(set(
                 list(net[net['ComponentIndex']==cid]['CLUSTERID1'])+
                 list(net[net['ComponentIndex']==cid]['CLUSTERID2'])
             ))
    for i in allcid:
        allfn = tabgnps[tabgnps['cluster index']==i].reset_index(drop=True).loc[0,'AllFiles']
        if mids is not None:
            gid = mids[mids['File']==fn]['gene_id'].reset_index(drop=True)[0]
            fn2 = list(mids[mids['gene_id']==gid]['File'])
        else:
            fn2 = fn
        if type(fn2) is not list:
            fn2 = [fn2]
        for f in fn2:
            if bool(re.search(f, allfn)):
                print(f+" Found it\n")

 
def main():
    parser = OptionParser(usage="usage: %prog [options]",
                          version="%prog 1.0")
    parser.add_option("-n", "--net",
                      help="GNPS network directory")
    parser.add_option("-c", "--gcfdf",
                      help="Gene cluster file",)
    parser.add_option("-m", "--gcfmap",
                      help="Gene cluster map",)
    parser.add_option("-M", "--meta",
                      help="Metadados connectando as tabelas anteriores",)
    parser.add_option("-o", "--out",
                      help="Output directory",)
    parser.add_option("-j", "--job",
                      default=None,
                      help="Optional GNPS job id",)
    parser.add_option("-p", "--plot",
                      default=0,
                      help="Plot tanglegram",)
    parser.add_option("-b", "--biom",
                      default=0,
                      help="Save tsv biom and metadados style format",)
    parser.add_option("-C", "--crude",
                      default=0,
                      help="Use only crude",)
    parser.add_option("-s", "--smet",
                      default=0,
                      help="Save matching metabolomics table",)
    parser.add_option("-S", "--sgen",
                      default=0,
                      help="Save matching gene cluster table",)
    parser.add_option("-t", "--thr",
                      default=1.0,
                      help="Jaccard similarity threshold to select metabolite-gene pairs",)

    (options, args) = parser.parse_args()

    gnps = options.net
    gcf_df = options.gcfdf
    gcf_map = options.gcfmap
    meta = options.meta
    out = options.out
    jobid = options.job
    plot = options.plot
    saveBiom = options.biom
    crude = options.crude
    saveFullmetab = options.smet
    saveFullgcf_df = options.sgen
    thr = options.thr

    if not os.path.exists(out):
         os.mkdir(out)
    else:
         sys.exit("The directory already exists!")
 
    # download gnps network with node attributes and edge list
    if jobid is not None:
         print('Downloading GNPS result\n')
         #jobid = '9e1b636f7a2f4a909ee695a416c270f0'
         url = "http://gnps.ucsd.edu/ProteoSAFe/DownloadResult?task=" + jobid + "&view=download_cluster_buckettable"
          
         response = requests.post(url)
          
         with open("download.zip", "wb") as output_file:
             for block in response.iter_content(1024):
                 output_file.write(block)
          
         if not os.path.exists(gnps):
             os.mkdir(gnps)
             zip_ref = zipfile.ZipFile('download.zip', 'r') 
             zip_ref.extractall(gnps)
             zip_ref.close()
     
    fls = glob.glob(gnps+'/*/*') 
    
    print('Loading the edge list and attributes table from GNPS network\n')
    f1 = np.where([bool(re.search('clusterinfosummarygroup_attributes_withIDs/', x)) for x in fls])[0][0] 
    tabgnps = pd.read_table(fls[f1])
    f2 = np.where([bool(re.search('networkedges_selfloop/', x)) for x in fls])[0][0] 
    net = pd.read_table(fls[f2])
    
    print('Create a presence/absence matrix with all clustered spectra and all samples\n')
    
    fnames = tabgnps['AllFiles'].apply(getSampID)
    usamp = np.array(list(set(list(itertools.chain.from_iterable(fnames)))))
    idx = fnames.apply(lambda a: set([np.where(usamp==b)[0][0] for b in a])) 
    prod = [(x, y) for x in range(len(idx)) for y in list(idx[x])] 
    
    r = [x for (x, y) in prod] # x_coordinate
    c = [y for (x, y) in prod] # y_coordinate
    data = [1] * len(r)
    m = scipy.sparse.coo_matrix((data, (r, c)), shape=(tabgnps.shape[0], len(usamp)))
    
    s = m.todense()
    
    ridx = np.array(tabgnps.index)
    ridx.shape = (tabgnps.shape[0], 1)
    s = np.append(ridx, s, 1)
    s = pd.DataFrame(s)
    s.columns = ['#SampID']+usamp.tolist()
    
    if saveBiom:
         print('Save the matrix with sample names in the first row for biom convertion\n')
         s.to_csv(out+'/feat_table.tsv', index=None, sep='\t')

         mapping = pd.DataFrame()
         mapping['#SampleID'] = s.columns[1:]
         mapping['BarcodeSequence'] = "GATACA" 
         mapping['LinkerPrimerSequence'] = "GATACA"
         mapping['CLASS'] = "dummy"
         mapping['Description'] = "Metabolome"
         mapping.to_csv(out+'/mapping_file_comparison.txt', index=None, sep='\t')
         mapping.head()
     
         
    # skip sampID header tag
    smolfamily = s[s.columns[1:]]
    smolfamily.index = list(tabgnps['cluster index'])
    
    print('Calculate the set of unique connected componets, including single nodes\n')
    ucomp = list(set(net['ComponentIndex']))
    
    print("Cluster molecules, such that, if at least one node",
          "of the connected component is present in a sample",
          "the sample has 1 assigned\n", sep="\n")

    rowlist = []
    
    for i in range(len(ucomp)):
        uidx = list(set(list(net[net['ComponentIndex']==ucomp[i]]['CLUSTERID1'])+
                    list(net[net['ComponentIndex']==ucomp[i]]['CLUSTERID2'])
                    )
               )
        rowlist.append(smolfamily.loc[uidx].apply(lambda a: 1 if sum(a)>0 else 0))
    
    s2 = pd.concat(rowlist, axis=1).T
    print("Check back correspondence of collapsed table") 
    fn = s2.columns[s2.loc[0]!=0][0] 
    check_fname(net=net, cid=ucomp[0], fn=fn, tabgnps=tabgnps)
    #check_fname(net=net, cid=5, fn=fn, tabgnps=tabgnps, mids=matching_ids2)
    
    # Can't match all ids with summary_list_2.txt below
    print("Load metagenomics files\n")
    gcf_df = pd.read_csv(gcf_df, sep='\t')
    gcf_df.index.names = ['GCFs']
    
    cn = [x for x in gcf_df.columns if len(x)==3]
    
    # Can't match all ids with summary_list_2.txt below
    gcf_map = pd.read_csv(gcf_map, sep='\t')
    gcf_map.index.names = ['GCFs']
    
    print("Load metadata files\n")
    meta = pd.read_table(meta)
    meta['File'] = [re.sub('\.d$', '', str(x)) for x in meta['File']]
    
    print("Match two letter id of metadata table with three letter id gene cluster table\n")
    present_cn_absent_locus = np.array(list(set(cn)-set(meta['Locus'])))
    present_locus_absent_cn = np.array(list(set(meta['Locus'])-set(cn)))
    
    mpos = [np.where(present_locus_absent_cn==re.sub('^.{1}', '', x)) for x in present_cn_absent_locus]
    
    cn = np.array(cn)
    for i in range(len(present_cn_absent_locus)):
        if len(mpos[i][0]):
            cn[cn==present_cn_absent_locus[i]] = re.sub('^.{1}', '', present_cn_absent_locus[i])
    
    dcn = pd.DataFrame(cn, columns=['gene_id'])
    
    print("Remove 4 number at beginning and space from MS file name\n")
    usamp2 = [re.sub('^\d+ ', '', x) for x in usamp]
    matching_ids = pd.merge(pd.DataFrame(usamp2), meta, left_on=0, right_on='File')
    
    print("MS files not present at metadata table\n")
    print(np.array(list(set(usamp2)-set(matching_ids['File']))))
    
    matching_ids2 = pd.merge(dcn, matching_ids, left_on='gene_id', right_on='Locus')
    
    print("Number of gene cluster ids matching with redundancy\n")
    print(len(set(matching_ids2['Locus'])))
    
    s2.columns = [re.sub('^\d+ ', '', x) for x in s2.columns]
    
    print("Collapse all MS sample (extract plus fractions) in one sample matching gene cluster",
          "If crude equals to 1, select only the crude extract when present\n", sep="\n")
    ulocus = list(set(matching_ids2['Locus']))
    slist = []
    cnlist = []
    mlist = []
    crude = 0
    # for debugging
    # c = 0
    
    for locus in ulocus:
        tmp = matching_ids2.loc[matching_ids2['Locus']==locus]
        tmp.reset_index(inplace=True, drop=True)
        if crude == 1 and sum(tmp['Fraction'].str.contains("crude", case=False)):
            tmp = tmp.loc[tmp['Fraction'].str.contains("crude", case=False)]
        if sum(tmp['Fraction'].str.contains("crude", case=False)):
            stmp = s2[tmp['File']].apply(lambda a: 1 if sum(a)>0 else 0, axis=1)
            tmp = tmp.loc[tmp['Fraction'].str.contains("crude", case=False)]
            tmp.drop_duplicates(subset=['Locus', 'File'], inplace=True)
            cnlist.append(list(tmp['File'])[0])
            mlist.append(tmp)
            slist.append(stmp)
            # for debugging
            #print((c,tmp.shape[0]))
            #c += 1
        else:
            stmp = s2[tmp['File']].apply(lambda a: 1 if sum(a)>0 else 0, axis=1)
            tmp = tmp.loc[[0]]
            cnlist.append(list(tmp['File'])[0])
            mlist.append(tmp)
            slist.append(stmp)
            # for debugging
            #print((c,tmp.shape[0]))
            #c += 1
    
    metab = pd.concat(slist, axis=1)
    metab.columns = cnlist

    print("Check back correspondence of collapsed extracts") 
    fn = metab.columns[metab.loc[0]!=0][0] 
    check_fname(net=net, cid=ucomp[0], fn=fn, tabgnps=tabgnps, mids=matching_ids2)
    
    metaidx = pd.concat(mlist)
    
    gcf_df2 = gcf_df[gcf_df.columns[[len(x)==3 for x in gcf_df.columns]]]
    gcf_df2.columns = cn
    gcf_df2 = gcf_df2[metaidx['Locus']]
    gcf_df2 = pd.concat([pd.DataFrame(list(range(gcf_df2.shape[0])), columns=['#SampleID']), gcf_df2], axis=1)
    
    metab = pd.concat([pd.DataFrame(list(ucomp), columns=['#SampleID']), metab], axis=1)
    
    print("Gene IDs duplicated in the table (probably because of name transformation above)")
    #print(metaidx['gene_id'][metaidx['gene_id'].duplicated()])
    print(gcf_df2.columns[gcf_df2.columns.duplicated()])
    print(metab.columns[metab.columns.duplicated()])
    
    print("Removing duplicates")
    lid1 = list(set(gcf_df2.columns)-set(gcf_df2.columns[gcf_df2.columns.duplicated()])) 
    lid2 = list(set(metab.columns)-set(metab.columns[metab.columns.duplicated()])) 
    idmatch = pd.merge(pd.DataFrame(lid1), metaidx, left_on=0, right_on='gene_id')
    idmatch = pd.merge(pd.DataFrame(lid2), idmatch, left_on=0, right_on='File')
    gcf_df2 = gcf_df2[['#SampleID']+list(idmatch['gene_id'])]
    metab = metab[['#SampleID']+list(idmatch['File'])]
    
    metab.columns = gcf_df2.columns
    
    if saveFullmetab:
         metab.to_csv(out+'/metabolome_for_comparison.txt', index=None, sep='\t')
    if saveFullgcf_df:
         gcf_df2.to_csv(out+'/metagenome_for_comparison.txt', index=None, sep='\t')
    
    print("Run Mantel test for full dataset")
    mmetab = metab[metab.columns[1:]].T
    mat1 = squareform(pdist(mmetab, 'jaccard'))
    mat1 = pd.DataFrame(mat1, columns=mmetab.index, index=mmetab.index)
    
    mgcf_df2 = gcf_df2[gcf_df2.columns[1:]].T
    mat2 = squareform(pdist(mgcf_df2, 'jaccard'))
    mat2 = pd.DataFrame(mat2, columns=mgcf_df2.index, index=mgcf_df2.index)
    
    #http://scikit-bio.org/docs/0.1.3/generated/skbio.math.stats.distance.mantel.html
    print("Correlation coefficient: %0.3f, p-value: %0.3f, Number of samples: %d" % mantel(mat1.as_matrix(),mat2.as_matrix())) 
    
    if plot:
        fig = tangle.plot(mat1,mat2)
        fig.savefig(out+'/tanglegram_whole.png')
    
    print("Generate all combinations of metabolites and gene clusters to test\n")
    idxlist = [(x,y) for x in list(metab.index) for y in list(gcf_df2.index)]

    # define a function to calculate similarity score 
    # (distance could be also used, in that case we should aim to 0 distance)
    #def myjaccard(idxtuple):
    #    return jaccard_similarity_score(metab.iloc[idxtuple[0]][1:], gcf_df2.iloc[idxtuple[1]][1:])
        
    print("Calculate jaccard_similarity_score for all combinations of metabolites and gene clusters\n")
    #pool = multiprocessing.Pool(4)
    #out = pool.map(myjaccard, idxlist)
    pr = product(metab[metab.columns[1:]].as_matrix(), gcf_df2[gcf_df2.columns[1:]].as_matrix()) 
    #result = [myjaccard(x) for x in idxlist]
    result = [jaccard_similarity_score(x[0], x[1]) for x in list(pr)]
    
    # Calculate all pairwise jaccard similarities of gene clusters VS metabolite clusters
    #sns.distplot(pd.DataFrame(result).dropna());
    
    print("Get the index of gene and metabolite clusters that",
           "have perfect correspondence in samples",
           "We can also change the selection changing the criteria",
           "to thr>=0.9 for example", sep="\n")

    metidx = []
    genidx = []
    for o in range(len(result)):
        if result[o]>=thr:
            metidx.append(idxlist[o][0])
            genidx.append(idxlist[o][1])
    
    metidx = list(set(metidx))
    genidx = list(set(genidx))
    
    print("Select the indexes and remove metabolites that are",
           "zero in all samples", sep="\n")
    mmetab = metab.iloc[metidx][metab.columns[1:]].T
    mmetab = mmetab[mmetab.columns[mmetab.apply(lambda a: sum(a)!=0)]]
    
    print("Select the indexes and remove genes that are"
          "zero in all samples", sep="\n")
    mgcf_df2 = gcf_df2.iloc[genidx][gcf_df2.columns[1:]].T
    mgcf_df2 = mgcf_df2[mgcf_df2.columns[mgcf_df2.apply(lambda a: sum(a)!=0)]]
    
    print("Find samples that have all zeros for selected metabolites")
    mmsamp = mmetab.apply(lambda a: sum(a)!=0, axis=1)
    mgsamp = mgcf_df2.apply(lambda a: sum(a)!=0, axis=1)
    nzerosamp = pd.concat([mmsamp, mgsamp], axis=1).apply(lambda a: sum(a)==2, axis=1)
    nzerosamp = [x for x in range(len(nzerosamp)) if nzerosamp[x]]
    
    mmetab = mmetab.iloc[nzerosamp]
    mgcf_df2 = mgcf_df2.iloc[nzerosamp]
    
    print("Run Mantel test for selected pairs")
    mat1 = squareform(pdist(mmetab, 'jaccard'))
    mat1 = pd.DataFrame(mat1, columns=mmetab.index, index=mmetab.index)
    
    mat2 = squareform(pdist(mgcf_df2, 'jaccard'))
    mat2 = pd.DataFrame(mat2, columns=mgcf_df2.index, index=mgcf_df2.index)

    print("Correlation coefficient: %0.3f, p-value: %0.3f, Number of samples: %d" % mantel(mat1.as_matrix(),mat2.as_matrix())) 
    
    if plot:
        fig = tangle.plot(mat1,mat2)
        fig.savefig(out+'/tanglegram_selected.png')
         
    print("Recover the metadata for all metabolite masses and gene clusters")
    cnm = [x for x in gcf_map.columns if len(x)==3] 
    gcf_map2 = gcf_map[cnm] 
    gcf_map2.columns = cn
    
    lann = []
    for sidx in mmetab.index: 
        l1 = list(mmetab.columns[np.where(mmetab.loc[[sidx]]!=0)[1]]) 
        l2 = list(mgcf_df2.columns[np.where(mgcf_df2.loc[[sidx]]!=0)[1]]) 
        for l11 in l1: 
            for l22 in l2: 
                if jaccard_similarity_score(mmetab[l11], mgcf_df2[l22])>=thr:
                    cid = metab['#SampleID'][l11] 
                    #gcf_map.loc[56,'3B2'] 
                    #gcf_df2.loc[56,'3B2'] 
                    allcid = list(set(
                                 list(net[net['ComponentIndex']==cid]['CLUSTERID1'])+
                                 list(net[net['ComponentIndex']==cid]['CLUSTERID2'])
                             ))
                    fn = list(matching_ids2[matching_ids2['gene_id']==sidx]['File'])
                    #matching_ids2[matching_ids2['gene_id']=='3B2'].reset_index(drop=True).loc[0, 'File']
                    for i in allcid:
                        allfn = tabgnps[tabgnps['cluster index']==i].reset_index(drop=True).loc[0,'AllFiles']
                        for f in fn:
                            if bool(re.search(f, allfn)):
                                #print("Found it")
                                lann.append(pd.concat([tabgnps[tabgnps['cluster index']==i][['cluster index','parent mass', 'AllFiles', 'LibraryID']].reset_index(drop=True),
                                                      matching_ids2[matching_ids2['File']==f][['gene_id', 'File']].reset_index(drop=True),
                                                      pd.DataFrame([[cid, gcf_map2.loc[l22,sidx]]], columns=['ComponentIndex', 'gcf_map'])], axis=1)
                                )
                    
            
    tann = pd.concat(lann)    
    tann = tann.drop_duplicates()   
    print("Save 'connection_table.tsv' with all metabolite masses and gene clusters")
    tann.to_csv(out+'/connection_table.tsv', index=None, sep='\t')

if __name__ == '__main__':
    start_time = time.time()
    main()
    print("--- %s seconds ---" % (time.time() - start_time))
