## A collection of scripts to process mass spectrometry data

To install and load the env do:

```
conda env create -f environment.yml
conda activate gnps 
```

To update the env, with the env activated, do:

```
conda env export | grep -v "^prefix: " > environment.yml
```

To use the jupyter notebook do:

```
jupyter notebook
```

and navigate to `selected_ref` folder.

