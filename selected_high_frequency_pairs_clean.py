import os
import pandas as pd
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import seaborn as sns
import readline

os.listdir('./res_scores')
f = open('./res_scores/log.txt').readlines()
import re
p = [x for x in range(len(f)) if bool(re.search('Mantel', f[x]))]
v = [f[x+1] for x in p]

v = [x.split(', ') for x in v]
pv = []
for x in v:
    tmp  = []
    for y in x:
        try:
            tmp += [float(re.sub('[^0-9.]', '', y)) ]
        except:
            tmp += [float('nan')]
    pv.append(tmp)

tv = pd.DataFrame(pv)

tv = pd.concat([tv.iloc[[20,21]], tv.iloc[list(range(0, 18))]]).reset_index(drop=True)

tv['threshold'] = [0.01, 0.01]+list(np.repeat(np.array(range(1,10))/10 , 2))
tv['set'] = ['Global', 'Selected']*10

with PdfPages('correlation_by_threshold.pdf') as pdf:
    ax = sns.pointplot(x="threshold", y=0, hue="set", data=tv)
    ax.set_title('Correlation by threshold')
    pdf.savefig()
    plt.close()
    ax = sns.pointplot(x="threshold", y=1, hue="set", data=tv)
    ax.set_title('p-values by threshold')
    pdf.savefig()
    plt.close()
    ax = sns.pointplot(x="threshold", y=2, hue="set", data=tv)
    ax.set_title('Number of samples by threshold')
    pdf.savefig()
    plt.close()

list(os.walk('res_scores'))

out_0001 = pd.read_table('res_scores/out_0.001/connection_table.tsv')

dflist = [out_0001]
[dflist.append(pd.read_table('res_scores/out_0.'+ str(x) +'/connection_table.tsv')) for x in range(1,10)]
dflist.append(pd.read_table('res_scores/out_0'+str(9)+'/connection_table.tsv'))

for x in range(10):
    dflist[x] = dflist[x][['cluster index', 'gcf_map']][np.invert(dflist[x][['cluster index', 'gcf_map']].duplicated())] 

dfmaster = pd.concat(dflist)
dfmaster.reset_index(drop=True, inplace=True)
fmaster = dfmaster.groupby(['cluster index', 'gcf_map']).size()

dfmaster[(dfmaster['cluster index']==703.0) & (dfmaster['gcf_map']=="['2F5_006']")].shape
dfmaster[(dfmaster['cluster index']==703.0) & (dfmaster['gcf_map']=="['2F5_019']")].shape

fmaster = fmaster.to_frame()
fmaster['cluster index'] = [fmaster.index[x][0] for x in range(fmaster.shape[0])]
fmaster['gcf_map'] = [fmaster.index[x][1] for x in range(fmaster.shape[0])]
fmaster.reset_index(drop=True, inplace=True)
sfmaster = fmaster[fmaster[0]==10]

dflist = [out_0001]
[dflist.append(pd.read_table('res_scores/out_0.'+ str(x) +'/connection_table.tsv')) for x in range(1,10)]
dflist.append(pd.read_table('res_scores/out_0'+str(9)+'/connection_table.tsv'))

dfmasterfull = pd.concat(dflist)
dfmasterfull = dfmasterfull[np.invert(dfmasterfull.duplicated())]

sfmaster['mid'] = sfmaster.apply(lambda a: str(a['cluster index'])+a['gcf_map'], axis=1)
dfmasterfull['mid'] = dfmasterfull.apply(lambda a: str(a['cluster index'])+str(a['gcf_map']), axis=1)
sfmasterfull = pd.merge(sfmaster, dfmasterfull, left_on='mid', right_on='mid')
fmaster.rename(columns={0:'Frequency'}, inplace=True)
fmaster.to_csv('ion_BGC_pair_frequency.tsv', sep='\t', index=None) 

sfmasterfull.rename(columns={'cluster index_x':'cluster index', 'gcf_map_x': 'gcf_map'}, inplace=True)
sfmasterfull[dflist[0].columns.tolist()].to_csv('selected_high_frequency_pairs.tsv', sep='\t', index=None)
readline.write_history_file('selected_high_frequency_pairs.py')
