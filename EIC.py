#!/home/labshare/anaconda3/envs/py27metab/bin/python

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
from pyopenms import *
import pandas as pd, numpy as np
import multiprocessing
import random
import re
import os
import argparse 

parser = argparse.ArgumentParser(description='Quantify list of peaks') 
parser.add_argument('-i', '--inp', type=str, help='input file name') 
parser.add_argument('-p', '--ppm', type=int, help='ppm error allowed') 
parser.add_argument('-r', '--rtwin', type=int, help='retention time window') 

args = parser.parse_args()

ppm = float(args.ppm)
rtwin = args.rtwin/2
fname = args.inp

fls = os.listdir('.')
files = [x for x in fls if 'mzML' in x]


def load_exp(fname):
    exp_in = MSExperiment()
    FileHandler().loadExperiment(fname, exp_in)
    return(exp_in)

exp = [load_exp(x) for x in files]



def runParallel(mol_list):

    molecule, mass, retention = mol_list
    mol_list = pd.DataFrame(index=[0], columns=['molecule', 'mass', 'retention time'])
    mol_list['molecule'] = molecule
    mol_list['mass'] = mass
    mol_list['retention time'] = retention

    mz = mol_list["mass"][0]/((ppm/(10**6))+1.0)

    mzdiff = abs(mz - mol_list["mass"][0]) 

    for f in files:
        mol_list[f] = 0

    for k in range(mol_list.shape[0]):
        mz2find = mol_list['mass'][k]

        count = 0
	all_peaks = []
        for exp_in in exp:

            time = []
            Intensities = []
            for j in range(exp_in.size()):
                spec = exp_in.getSpectrum(j)
                it = 0
                if spec.getMSLevel() == 1:
                    rt = spec.getRT()
                    if rt > mol_list['retention time'][k]-rtwin and rt < mol_list['retention time'][k]+rtwin:
                        #p = np.where([y == mz2find for y in [round(x, rfactor) for x in spectrum.mz]])[0]
                        pks = spec.get_peaks()
                        p = np.where([y > mz2find-mzdiff and y < mz2find+mzdiff for y in list(pks[0])])[0]
                        original_peaks = [list(pks[1])[i] for i in p]
                        it = sum(original_peaks)
                        
                        time.append(rt)
                        Intensities.append(it)
                        it = 0
                    else:
                        time.append(rt)
                        Intensities.append(0)

            all_peaks.append([time, Intensities])
            mol_list[files[count]][k] = sum(Intensities)
            count += 1
            
   

    return([mol_list, all_peaks])


if __name__ == '__main__':
    clist =[]
    for name, hex in matplotlib.colors.cnames.items():
    	clist.append(hex)

    clist = np.array(clist)   

    fls = os.listdir('.')
    files = [x for x in fls if 'mzML' in x]

    #mol_list = pd.read_csv('Pseudomonas_molecule_mzrt_list.csv')
    mol_list = pd.read_csv(fname)
    z= zip(mol_list['molecule'], mol_list['mass'], mol_list['retention time'])
    
    pool = multiprocessing.Pool()
    results = pool.map(runParallel, z)

    all_peaks = [x[1] for x in results] 

    pdf = matplotlib.backends.backend_pdf.PdfPages(re.sub('.csv', '.pdf', fname))

    for k in range(mol_list.shape[0]):
	#cvec = ["blue", "red"]
	fig = plt.figure()
	fig.suptitle(mol_list['molecule'][k], fontsize=12)

	for l in range(len(files)):
		cvec = clist[random.sample(range(0, len(clist)), 1)]
		ax1 = fig.add_subplot(111)
		ax1.plot(all_peaks[k][l][0], all_peaks[k][l][1], color=cvec[0])

	pdf.savefig( fig )	
	plt.close(fig)

	
    pdf.close()
    result = pd.concat([x[0] for x in results])
    result.to_csv(re.sub('.csv', 'output.csv', fname))
