from pyopenms import *
import subprocess 
import pandas as pd
import re

exp = MSExperiment()
MzMLFile().load('JHB_C_GA7_01_37727.mzML', exp)
spectrum_data = exp.getSpectrum(0).get_peaks()
spec = []
for s in exp.getSpectra():
    if s.getMSLevel() == 1:
        spec.append(s)

exp.getNrSpectra()
#exp.getNrChromatograms()

def calcTIC(exp):
     tic = 0
     for spec in exp:
         if spec.getMSLevel() == 1:
             mz, i = spec.get_peaks()
             tic += sum(i)
     return tic

calcTIC(exp)

def getTIC(exp):
     tic = {} 
     lmz = []
     li = []
     rt = []
     for spec in exp:
         if spec.getMSLevel() == 1:
             mz, i = spec.get_peaks()
             lmz.append(mz)
             li.append(i)
             rt.append(spec.getRT())
     tic['mz'] = lmz
     tic['i'] = li
     tic['rt'] = rt 
     return tic

tic = getTIC(exp)

import pandas as pd
import os
conn = pd.read_table('connection_table_without_analogs.tsv')

gnps = pd.read_table('gnps_res_without_analogs/clusterinfosummarygroup_attributes_withIDs/0ebbc0148229427aabc6802c67c0833d..out')

gnps[['parent mass','cluster index','RTMean']].iloc[np.where(gnps['cluster index']==1520522)]

def getXIC(mass, retention, tic, type='BP', rttol=20, ppm=20):
    xic = {} 
    mztol = mass/((ppm/(10**6))+1.0)
    mzdiff = abs(mztol - mass) 
    rt = np.array(tic['rt']) 
    rtidx = np.where((rt>retention-rttol) & (rt<retention+rttol)) 
    lmz = []
    lrt = []
    li = []
    lxic = []
    for idx in rtidx[0]:
        lrt.append(rt[idx])
        mztemp = np.array(tic['mz'][idx]) 
        itemp = np.array(tic['i'][idx]) 
        mzidx = np.where((mztemp>mass-mzdiff) & (mztemp<mass+mzdiff)) 
        if len(mzidx[0]):
            lmz += list(mztemp[mzidx])
            li += list(itemp[mzidx])
            if type=='TI':
                lxic.append(sum(itemp[mzidx]))
            elif type=='BP':
                if len(itemp[mzidx]):
                    lxic.append(max(itemp[mzidx]))
                else:
                    lxic.append(0)
        else:
            lxic.append(0)
    xic['mz'] = lmz
    xic['rt'] = lrt
    xic['i'] = li
    xic['xic'] = lxic
    return xic
 
xic = getXIC(1105.35, 410.6825, tic)
import matplotlib.pyplot as plt
plt.plot(xic['rt'], xic['xic'], lw=4)

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf

import matplotlib.backends.backend_pdf
fig = plt.figure()
fig.suptitle('test', fontsize=12)
ax1 = fig.add_subplot(111)
import readline
readline.write_history_file('eic_script.py')

import matplotlib

quant = []

repo1 = '/home/rsilva/Desktop/coral_sio/cyanobiome_subset_mzXML/'
repo2 = '/home/rsilva/Desktop/coral_sio/old_lock_mass_corrected/'

for i in range(conn.shape[0]):
    if conn['cluster index'].isnull()[i]:
        continue
    fls = re.split(':\d+###', conn['AllFiles'][i]) 
    fls = list(set(fls))    
    fls = [x for x in fls if x!=''] 
    for f in fls:
        if os.path.isfile(re.sub('mzXML$', 'mzML', f)):
             pass
        elif  os.path.isfile(repo1+f):
             subprocess.run(['FileConverter', '-in', repo1+f, '-out', re.sub('mzXML$', 'mzML', f)]) 
             #os.system('FileConverter -in '+repo1+f+' -out '+re.sub('mzXML$', 'mzML', f)) 
        elif  os.path.isfile(repo2+f):
             subprocess.run(['FileConverter', '-in', repo2+f, '-out', re.sub('mzXML$', 'mzML', f)]) 
             #os.system('FileConverter -in '+repo2+f+' -out '+re.sub('mzXML$', 'mzML', f)) 
        exp = MSExperiment()
        MzMLFile().load(re.sub('mzXML$', 'mzML', f), exp)
        TIC = calcTIC(exp)
        tic = getTIC(exp)
        rt = gnps['RTMean'][np.where(gnps['cluster index']==conn['cluster index'][i])[0][0]]
        xic = getXIC(conn['parent mass'][i], rt, tic, type='TI')
        exp.reset()
        quant.append([conn['cluster index'][i], TIC, sum(xic['xic'])])


mzfls = []


for i in range(conn.shape[0]):
    if conn['cluster index'].isnull()[i]:
        continue
    fls = re.split(':\d+###', conn['AllFiles'][i]) 
    fls = list(set(fls))    
    fls = [x for x in fls if x!=''] 
    rt = gnps['RTMean'][np.where(gnps['cluster index']==conn['cluster index'][i])[0][0]]
    for f in fls:
         mzfls.append([i, f, rt])
 
mzfls = pd.DataFrame(mzfls, columns=['idx', 'File', 'RTMean']) 

dquant = pd.DataFrame(quant, columns=['cluster index', 'TIC', 'XIC']) 
dquant['rel_quant'] = dquant['XIC']/dquant['TIC'] 
dquant = pd.concat([mzfls, dquant], axis=1) 
dquant.sort_values(by=['rel_quant'], ascending=False).head() 
dquant[np.invert(dquant.duplicated())].shape 

conn.reset_index(inplace=True) 
dquant = pd.merge(dquant, conn, left_on='idx', right_on='index') 

os.listdir('nap/final_out')
nap = pd.read_table('nap/final_out/node_attributes_table.tsv')
snap = nap[['parent.mass', 'RTMean', 'LibraryID', 'MetFragID', 'FusionID', 'ConsensusID']]
snap.fillna('', inplace=True)

dquant['InSilico'] = '' 
duplicated = []
missing = []

for i in range(snap.shape[0]):
    if snap.loc[i, 'FusionID']!='':
        #ss = set(dquant[(round(dquant['parent mass'], 4)==round(snap.loc[i, 'parent.mass'],4))]['cluster index_x']) 
        ss = set(dquant[dquant['parent mass']==snap.loc[i, 'parent.mass']]['cluster index_x']) 
        if len(ss)==1:
            #dquant.loc[(round(dquant['parent mass'])==round(snap.loc[i, 'parent.mass'])), 'InSilico'] =  snap.loc[i, 'FusionID']
            dquant.loc[dquant['parent mass']==snap.loc[i, 'parent.mass'], 'InSilico'] =  snap.loc[i, 'FusionID']
        elif len(ss)>1:
            duplicated.append(i)  
        else:
            missing.append(i)  
    elif snap.loc[i, 'ConsensusID']!='':
        ss = set(dquant[dquant['parent mass']==snap.loc[i, 'parent.mass']]['cluster index_x']) 
        if len(ss)==1:
            dquant.loc[dquant['parent mass']==snap.loc[i, 'parent.mass'], 'InSilico'] =  snap.loc[i, 'ConsensusID']
        elif len(ss)>1:
            duplicated.append(i)  
        else:
            missing.append(i)  
    elif snap.loc[i, 'MetFragID']!='':
        ss = set(dquant[dquant['parent mass']==snap.loc[i, 'parent.mass']]['cluster index_x']) 
        if len(ss)==1:
            dquant.loc[dquant['parent mass']==snap.loc[i, 'parent.mass'], 'InSilico'] =  snap.loc[i, 'MetFragID']
        elif len(ss)>1:
            duplicated.append(i)  
        else:
            missing.append(i)  
            
    

task = 'c8e426a9721746b0a7a2814bd0b5395d'

def getComponentlink(comp, taskid):
    burl = 'https://gnps.ucsd.edu/ProteoSAFe/result.jsp?view=network_displayer&componentindex='
    purl = '&task='+taskid+'#%7B%7D'
    #return burl+str(comp)+purl
    return '<a href=\"'+ burl+str(comp)+purl+ '\" target=\"_blank\">'+str(comp)+'</a>'

dquant['CompLink'] = dquant['ComponentIndex'].apply(lambda a: getComponentlink(int(a), task)) 


dquant.to_csv('quantification_table.tsv', sep='\t', index=None) 

def getDBlink(id):
    if bool(re.match('^\d{3,}', id)):
    # provisory for marinlit
        id2 = id.zfill(12)
        url = "http://pubs.rsc.org/marinlit/compound/cs"+id2
        return '<a href=\"'+ url + '\" target=\"_blank\">'+id+'</a>'
    else:
        #return "http://dnp.chemnetbase.com/AAA00.entry?parentCHNumber="+id+"&exno="+id
        # http://dnp.chemnetbase.com/faces/chemical/FullScreenEntry.xhtml?product=DNP&fromExport=falsePOSIBBB04&id=BBB04
        url = "https://pubchem.ncbi.nlm.nih.gov/search/#collection=compounds&query_type=text&query="+id
        return '<a href=\"'+ url + '\" target=\"_blank\">'+id+'</a>'


def getLibraIDlink(i, tab):
    burl = 'https://gnps.ucsd.edu/ProteoSAFe/result.jsp?task='
    purl = '&view=view_all_annotations_DB#%7B%22main.Compound_Name_input%22%3A%22'
    eurl = '%22%7D'
    gid = tab.loc[0,'ProteoSAFeClusterLink'].split('&')[0].split('=')[1]
    if i!='':
        return '<a href=\"'+ burl+gid+purl+str(i)+eurl+ '\" target=\"_blank\">'+str(i)+'</a>'
    else:       
        return ''


dquant.fillna('', inplace=True) 
dquant['LibraryID'] = dquant['LibraryID'].apply(lambda a: '#'.join([getLibraIDlink(x, gnps) for x in list(a.split(','))]) if a!='' else '') 
dquant['InSilico'] = dquant['InSilico'].apply(lambda a: '#'.join([getDBlink(x) for x in list(a.split(','))]) if a!='' else '')

comp = list(set(dquant['ComponentIndex']))
mgffile = 'gnps_res_without_analogs/METABOLOMICS-SNETS-c8e426a9-download_clustered_spectra-main.mgf'
net = pd.read_table('gnps_res_without_analogs/networkedges/ce13e313b2314b66b0806c7dbb02ad09.pairsinfo', header=None)
f = open(mgffile)
lines = np.array(f.readlines())
f.close()
bg = np.where(lines=='BEGIN IONS\n')
ed = np.where(lines=='END IONS\n')
thefile = open('outfile.mgf', 'w+')
 

for c in comp:
    netnodes = list(net[net[6]==c][0])+list(net[net[6]==c][1])
    for scanindex in netnodes: 
        p = np.where(lines=='SCANS='+str(scanindex)+'\n')
        bgp = bg[0][np.where((bg[0]-p[0])>0)[0][0]-1]
        edp = ed[0][np.where((ed[0]-p[0])>0)[0][0]]+1
        thefile.write(''.join(lines[bgp:edp])) 
        thefile.write('\n') 

thefile.close() 

def getSingleSpectrum(mgffile, scanindex):
    spectrum = {}
    f = open(mgffile)
    lines = np.array(f.readlines())
    f.close()
    bg = np.where(lines=='BEGIN IONS\n')
    ed = np.where(lines=='END IONS\n')
    p = np.where(lines=='SCANS='+str(scanindex)+'\n')
    bgp = bg[0][np.where((bg[0]-p[0])>0)[0][0]-1]
    edp = ed[0][np.where((ed[0]-p[0])>0)[0][0]]+1
    msms1 = []
    for line in lines[bgp:edp]:
        try:
            mz, rt = line.split(' ')
            msms1.append((float(mz), float(rt)))
        except:
            pass
    spectrum[scanindex] = pd.DataFrame(msms1, columns=['Mass', 'Intensity'])
    return(spectrum)




def plotXIC(mz, rt, file, out, type='XIC', format='png'):
    exp = MSExperiment()
    MzMLFile().load(file, exp)
    tic = getTIC(exp)
    xic = getXIC(mz, rt, tic, type='TI')
    if format=='pdf':
        pdf = matplotlib.backends.backend_pdf.PdfPages(out+'.pdf') 
    if type=='BPC':
        fn = str(mz)+'_'+str(rt)+'_'+re.sub('\.mzXML', '', file)+'_BPC'
        fig = plt.figure() 
        fig.suptitle(fn, fontsize=12)
        ax1 = fig.add_subplot(111) 
        ax1.plot(tic['rt'], [max(x) if len(x) else 0 for x in tic['i']], lw=2) 
        if format=='png':
            fig.savefig(out+'.png')
        elif format=='pdf':
            pdf.savefig( fig )   
            pdf.close() 
        plt.close('all') 
    if type=='BPC_XIC':
        fn = str(mz)+'_'+str(rt)+'_'+re.sub('\.mzXML', '', file)+'_BPC_XIC'
        fig = plt.figure() 
        fig.suptitle(fn, fontsize=12)
        ax1 = fig.add_subplot(111) 
        ax1.plot(tic['rt'], [max(x) if len(x) else 0 for x in tic['i']], lw=2) 
        ax1.plot(xic['rt'], xic['xic'], lw=2, color='r') 
        if format=='png':
            fig.savefig(out+'.png')
        elif format=='pdf':
            pdf.savefig( fig )   
            pdf.close() 
        plt.close('all') 
    if type=='XIC':
        fn = str(mz)+'_'+str(rt)+'_'+re.sub('\.mzXML', '', file)+'_XIC'
        fig = plt.figure() 
        ax1 = fig.add_subplot(111) 
        fig = plt.figure() 
        fig.suptitle(fn, fontsize=12)
        ax1 = fig.add_subplot(111) 
        ax1.plot(xic['rt'], xic['xic'], lw=4, color='r') 
        if format=='png':
            fig.savefig(out+'.png')
        elif fot=='pdf':
            pdf.savefig( fig )   
            pdf.close() 
        plt.close('all') 
    exp.reset()

os.mkdir('quant_plot') 

for i in range(conn.shape[0]):
    if conn['cluster index'].isnull()[i]:
        continue
    fls = re.split(':\d+###', conn['AllFiles'][i]) 
    fls = list(set(fls))    
    fls = [x for x in fls if x!=''] 
    for f in fls:
        tic = getTIC(exp)
        rt = gnps['RTMean'][np.where(gnps['cluster index']==conn['cluster index'][i])[0][0]]
        mz = conn['parent mass'][i]
        fn = 'quant_plot/'+re.sub('mzXML', '', f)+'_'+str(i)+'XIC'
        plotXIC(mz, rt, re.sub('mzXML', 'mzML', f), out=fn, type='XIC', format='png') 
        fn = 'quant_plot/'+re.sub('mzXML', '', f)+'_'+str(i)+'BPC_XIC'
        plotXIC(mz, rt, re.sub('mzXML', 'mzML', f), out=fn, type='BPC_XIC', format='png') 

fns = []
for i in range(conn.shape[0]):
    if conn['cluster index'].isnull()[i]:
        continue
    fls = re.split(':\d+###', conn['AllFiles'][i]) 
    fls = list(set(fls))    
    fls = [x for x in fls if x!=''] 
    for f in fls:
        fn1 = 'quant_plot/'+re.sub('mzXML', '', f)+'_'+str(i)+'XIC.png'
        fn2 = 'quant_plot/'+re.sub('mzXML', '', f)+'_'+str(i)+'BPC_XIC.png'
        fns.append([i, f, fn1, fn2])


sum(dquant['idx']==dfns['fidx']) 

dfns = pd.DataFrame(fns, columns=['fidx', 'File_name', 'xic_plot', 'bpc_xic_plot'] )
dquant = pd.concat([dquant, dfns], axis=1)


dquant.drop(['File_x', 'cluster index_x', 'AllFiles', 'level_0', 'idx', 'fidx'], axis=1, inplace=True) 
dquant = dquant[['cluster index_y', 'parent mass', 'RTMean', 'TIC', 'XIC', 'rel_quant', 'LibraryID', 'InSilico', 'CompLink', 'File_name', 'gcf_map', 'File_name', 'xic_plot', 'bpc_xic_plot']] 


dquant['xic_plot'] = dfns['xic_plot']
dquant['bpc_xic_plot'] = dfns['bpc_xic_plot']
dquant['xic_plot'] = dquant['xic_plot'].apply(lambda a: re.sub(' ', '%20', a) )
dquant['bpc_xic_plot'] = dquant['bpc_xic_plot'].apply(lambda a: re.sub(' ', '%20', a))

dquant['xic_plot'] = dquant['xic_plot'].apply(lambda a: '<img src=\"'+a+'" />')
dquant['bpc_xic_plot'] = dquant['bpc_xic_plot'].apply(lambda a: '<img src=\"'+a+'" />')
dquant['xic_plot'] = dquant['xic_plot'].apply(lambda a: re.sub('quant', './quant', a) )
dquant['bpc_xic_plot'] = dquant['bpc_xic_plot'].apply(lambda a: re.sub('quant', './quant', a))
dquant['xic_plot'] = dquant['xic_plot'].apply(lambda a: re.sub('>$', 'width=\"240\" height=\"200\">', a) )
dquant['bpc_xic_plot'] = dquant['bpc_xic_plot'].apply(lambda a: re.sub('>$', 'width=\"240\" height=\"200\">', a))

dquant.to_csv('quantification_partial_table.txt', sep='\t', index=None) 
