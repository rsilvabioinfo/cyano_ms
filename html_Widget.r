args = commandArgs(trailingOnly=TRUE)

library("DT")

tab <- read.delim(args[1])
html <- datatable(tab, escape = FALSE)
saveWidget(html, sub("\\.txt$", "\\.html", args[1]))
