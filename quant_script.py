from pyopenms import *
import subprocess 
import pandas as pd
import os 
import re
import time
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
from optparse import OptionParser

def getTIC(exp):
     tic = {} 
     lmz = []
     li = []
     rt = []
     for spec in exp:
         if spec.getMSLevel() == 1:
             mz, i = spec.get_peaks()
             lmz.append(mz)
             li.append(i)
             rt.append(spec.getRT())
     tic['mz'] = lmz
     tic['i'] = li
     tic['rt'] = rt 
     return tic

def getXIC(mass, retention, tic, type='BP', rttol=20, ppm=20):
    xic = {} 
    mztol = mass/((ppm/(10**6))+1.0)
    mzdiff = abs(mztol - mass) 
    rt = np.array(tic['rt']) 
    rtidx = np.where((rt>retention-rttol) & (rt<retention+rttol)) 
    lmz = []
    lrt = []
    li = []
    lxic = []
    for idx in rtidx[0]:
        lrt.append(rt[idx])
        mztemp = np.array(tic['mz'][idx]) 
        itemp = np.array(tic['i'][idx]) 
        mzidx = np.where((mztemp>mass-mzdiff) & (mztemp<mass+mzdiff)) 
        if len(mzidx[0]):
            lmz += list(mztemp[mzidx])
            li += list(itemp[mzidx])
            if type=='TI':
                lxic.append(sum(itemp[mzidx]))
            elif type=='BP':
                if len(itemp[mzidx]):
                    lxic.append(max(itemp[mzidx]))
                else:
                    lxic.append(0)
        else:
            lxic.append(0)
    xic['mz'] = lmz
    xic['rt'] = lrt
    xic['i'] = li
    xic['xic'] = lxic
    return xic
 

gnps_nodes = pd.read_table('gnps_diversity_nodes-TFL180829') 
conn = gnps_nodes[['cluster index', 'parent mass', 'AllFiles', 'RTMean']]


quant = []

mzml = '..' 
repo = ['/home/rsilva/Desktop/coral_sio/cyanobiome_subset_mzXML/','/home/rsilva/Desktop/coral_sio/old_lock_mass_corrected/']

dtic = {}

repo1 = repo[0] 
if len(repo)>1:
    repo2 = repo[1]  

for i in range(conn.shape[0]):
    if conn['cluster index'].isnull()[i]:
        continue
    fls = re.split(':\d+###', conn['AllFiles'][i]) 
    fls = list(set(fls))    
    fls = [x for x in fls if x!=''] 
    for f in fls:
        if os.path.isfile(mzml+'/'+re.sub('mzXML$', 'mzML', f)):
             pass
        elif  os.path.isfile(repo1+f):
             subprocess.run(['FileConverter', '-in', repo1+f, '-out', os.path.join(mzml, re.sub('mzXML$', 'mzML', f))]) 
             #os.system('FileConverter -in '+repo1+f+' -out '+re.sub('mzXML$', 'mzML', f)) 
        elif  os.path.isfile(repo2+f):
             subprocess.run(['FileConverter', '-in', repo2+f, '-out', os.path.join(mzml, re.sub('mzXML$', 'mzML', f))]) 
             #os.system('FileConverter -in '+repo2+f+' -out '+re.sub('mzXML$', 'mzML', f)) 
        exp = MSExperiment()
        MzMLFile().load(re.sub('mzXML$', 'mzML', os.path.join(mzml, f)), exp)
        #TIC = calcTIC(exp)
        if i==0:
            dnames = np.array([''])
        else:
            dnames = np.array(list(dtic.keys()))
        if sum(dnames==f):
            tic = dtic[f] 
        else:
            tic = getTIC(exp)
            dtic[f] = tic 
        rt = conn['RTMean'][i]
        exp.reset()
        nxic = f+'_'+str(conn['cluster index'][i])
        if sum(dnames==nxic):
            xic = dtic[nxic] 
        else:
            xic = getXIC(conn['parent mass'][i], rt, tic, type='TI')
            dtic[nxic] = xic 
        TIC = sum(np.concatenate(tic['i']))
        quant.append([conn['cluster index'][i], TIC, sum(xic['xic'])])

mzfls = []

for i in range(conn.shape[0]):
    if conn['cluster index'].isnull()[i]:
        continue
    fls = re.split(':\d+###', conn['AllFiles'][i]) 
    fls = list(set(fls))    
    fls = [x for x in fls if x!=''] 
    rt = conn['RTMean'][i]
    for f in fls:
         mzfls.append([i, f, rt])
 
mzfls = pd.DataFrame(mzfls, columns=['idx', 'File', 'RTMean']) 
 
dquant = pd.DataFrame(quant, columns=['cluster index', 'TIC', 'XIC']) 
dquant['rel_quant'] = dquant['XIC']/dquant['TIC'] 
dquant = pd.concat([mzfls, dquant], axis=1) 
#dquant.sort_values(by=['rel_quant'], ascending=False).head() 
#dquant[np.invert(dquant.duplicated())].shape 

conn.reset_index(inplace=True) 
dquant = pd.merge(dquant, conn, left_on='idx', right_on='index') 
 

snap = nap[['parent.mass',  'MetFragID', 'MetFragSMILES', 'FusionID', 'FusionSMILES', 'ConsensusID', 'ConsensusSMILES']]
snap.fillna('', inplace=True)

dquant['InSilicoID'] = '' 
dquant['InSilicoSMILES'] = '' 
duplicated = []
missing = []

for i in range(snap.shape[0]):
    if snap.loc[i, 'FusionID']!='':
        #ss = set(dquant[(round(dquant['parent mass'], 4)==round(snap.loc[i, 'parent.mass'],4))]['cluster index_x']) 
        ss = set(dquant[dquant['parent mass']==snap.loc[i, 'parent.mass']]['cluster index_x']) 
        if len(ss)==1:
            #dquant.loc[(round(dquant['parent mass'])==round(snap.loc[i, 'parent.mass'])), 'InSilico'] =  snap.loc[i, 'FusionID']
            dquant.loc[dquant['parent mass']==snap.loc[i, 'parent.mass'], 'InSilicoID'] =  snap.loc[i, 'FusionID']
            dquant.loc[dquant['parent mass']==snap.loc[i, 'parent.mass'], 'InSilicoSMILES'] =  snap.loc[i, 'FusionSMILES']
        elif len(ss)>1:
            duplicated.append(i)  
        else:
            missing.append(i)  
    elif snap.loc[i, 'ConsensusID']!='':
        ss = set(dquant[dquant['parent mass']==snap.loc[i, 'parent.mass']]['cluster index_x']) 
        if len(ss)==1:
            dquant.loc[dquant['parent mass']==snap.loc[i, 'parent.mass'], 'InSilicoID'] =  snap.loc[i, 'ConsensusID']
            dquant.loc[dquant['parent mass']==snap.loc[i, 'parent.mass'], 'InSilicoSMILES'] =  snap.loc[i, 'ConsensusSMILES']
        elif len(ss)>1:
            duplicated.append(i)  
        else:
            missing.append(i)  
    elif snap.loc[i, 'MetFragID']!='':
        ss = set(dquant[dquant['parent mass']==snap.loc[i, 'parent.mass']]['cluster index_x']) 
        if len(ss)==1:
            dquant.loc[dquant['parent mass']==snap.loc[i, 'parent.mass'], 'InSilicoID'] =  snap.loc[i, 'MetFragID']
            dquant.loc[dquant['parent mass']==snap.loc[i, 'parent.mass'], 'InSilicoSMILES'] =  snap.loc[i, 'MetFragSMILES']
        elif len(ss)>1:
            duplicated.append(i)  
        else:
            missing.append(i)  
 

gnps_nodes['InSilicoID'] = '' 
gnps_nodes['InSilicoSMILES'] = '' 
duplicated = []
missing = []

for i in range(snap.shape[0]):
    if snap.loc[i, 'FusionID']!='':
        #ss = set(gnps_nodes[(round(gnps_nodes['parent mass'], 4)==round(snap.loc[i, 'parent.mass'],4))]['cluster index_x']) 
        ss = set(gnps_nodes[gnps_nodes['parent mass']==snap.loc[i, 'parent.mass']]['cluster index']) 
        if len(ss)==1:
            #gnps_nodes.loc[(round(gnps_nodes['parent mass'])==round(snap.loc[i, 'parent.mass'])), 'InSilico'] =  snap.loc[i, 'FusionID']
            gnps_nodes.loc[gnps_nodes['parent mass']==snap.loc[i, 'parent.mass'], 'InSilicoID'] =  snap.loc[i, 'FusionID']
            gnps_nodes.loc[gnps_nodes['parent mass']==snap.loc[i, 'parent.mass'], 'InSilicoSMILES'] =  snap.loc[i, 'FusionSMILES']
        elif len(ss)>1:
            duplicated.append(i)  
        else:
            missing.append(i)  
    elif snap.loc[i, 'ConsensusID']!='':
        ss = set(gnps_nodes[gnps_nodes['parent mass']==snap.loc[i, 'parent.mass']]['cluster index']) 
        if len(ss)==1:
            gnps_nodes.loc[gnps_nodes['parent mass']==snap.loc[i, 'parent.mass'], 'InSilicoID'] =  snap.loc[i, 'ConsensusID']
            gnps_nodes.loc[gnps_nodes['parent mass']==snap.loc[i, 'parent.mass'], 'InSilicoSMILES'] =  snap.loc[i, 'ConsensusSMILES']
        elif len(ss)>1:
            duplicated.append(i)  
        else:
            missing.append(i)  
    elif snap.loc[i, 'MetFragID']!='':
        ss = set(gnps_nodes[gnps_nodes['parent mass']==snap.loc[i, 'parent.mass']]['cluster index']) 
        if len(ss)==1:
            gnps_nodes.loc[gnps_nodes['parent mass']==snap.loc[i, 'parent.mass'], 'InSilicoID'] =  snap.loc[i, 'MetFragID']
            gnps_nodes.loc[gnps_nodes['parent mass']==snap.loc[i, 'parent.mass'], 'InSilicoSMILES'] =  snap.loc[i, 'MetFragSMILES']
        elif len(ss)>1:
            duplicated.append(i)  
        else:
            missing.append(i)  
 
